__author__ = 'paolobruzzo'
'''
This script retrieves the same web page N times,
to collect the N query results
'''

import urllib2
import numpy as np
import time

import mysql.connector
from mysql.connector import errorcode

# Used to flush the database at each query run
def flushCacheMySQL():
    dbConfig = {
        'user': "root",
        'password': "root",
        'host': "127.0.0.1",
        'port': 3306,
        'database': "bookstore100K"
    }
    try:
        conn = mysql.connector.connect(**dbConfig)
        cursor = conn.cursor()
        cursor.execute("RESET QUERY CACHE")
        cursor.close()
        conn.close()

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print "Something is wrong with your username or password"
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print "Database does not exist"
        else:
            print "Hostname or port are incorrect"


def current_millisec():
    return time.time() * 1000

# Get url for 'numberOfRequests' times
def test(url, numberOfRequests):
    total_times = []

    for i in range(1, numberOfRequests + 1):
        nf = urllib2.urlopen(url)
        start = current_millisec()

        fb = nf.read()
        total_time = current_millisec() - start

        nf.close()
        if nf.getcode() != 200:
            print "Response not OK"
            exit(1)

        total_times.append(total_time)

    avg = np.average(np.array(total_times))

    # Correct outliers
    # for val in values:
    # if val > 2 * avg:
    #         print "!!! Outlier removed: "+str(val)
    #         values.remove(val)

    avg = np.average(np.array(total_times))
    stdev = np.std(np.array(total_times))

    return avg, stdev


if __name__ == '__main__':

    ########## INPUTS to edit ########

    repeat = 500
    requestsPerCicle = 1
    url = 'http://localhost:8080/Bookstore/Books.jsp?category_id=&name=a'

    ##################################

    avgValues = []
    stdevValues = []
    for i in range(1, repeat + 1):
        flushCacheMySQL()
        avg, stdev = test(url, requestsPerCicle)

        avgValues.append(avg)
        stdevValues.append(stdev)
        print "Executed test %d/%d" % (i, repeat)
        # print "\n----------TEST "+str(i)+", "+str(requestsPerCicle)+" requests --------------"
        # print "Average time to page load: " + str(round(avg, 3)) + " ms"
        # print "Stdev time to page load: " + str(round(stdev, 3)) + " ms"

    # print "\n=============OVERALL over "+str(repeat*requestsPerCicle)+" requests ================"
    #print "Overall Average time to page load= " + str(round(np.average(np.array(avgValues)), 3)) + " ms"
    #print "Overall Stdev time to page load= " + str(round(np.average(np.array(stdevValues)), 3)) + " ms"
    #print "======================================================="
class Keywords:
    MIN_DATE = "1970-01-01 00:00:00"
    MAX_DATE = "2100-01-01 00:00:00"
    DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"

    WARNING_IMAGE = ":/icons/icons/warning.png"
    ACCEPTED_IMAGE = ":/icons/icons/accept.png"
    DENYED_IMAGE = ":/icons/icons/deny.png"
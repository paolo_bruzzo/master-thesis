from PySide.QtGui import QMainWindow
from PySide.QtGui import QTableWidgetItem
from PySide.QtGui import QAction
from PySide.QtCore import Qt
from PySide.QtGui import QFileDialog

from sources.view import PoliciesOverview
from sources.model.entities.SimplePolicy import SimplePolicy
from sources.model.entities.AggregatePolicy import AggregatePolicy
from sources.model.XmlWriter import XmlWriter
from sources.utils.TabGenerator import TabGenerator
from sources.utils.Keywords import Keywords
from sources.controller.SimplePolicyCreatorController import SimplePolicyCreatorController
from sources.controller.AggregatePolicyCreatorController import AggregatePolicyCreatorController
from sources.controller.subcomponents.ConfirmationDialogController import ConfirmationDialogController


class PoliciesOverviewController(QMainWindow, PoliciesOverview.Ui_MainWindow):
    def __init__(self, mainController=None, parentUI=None):
        super(PoliciesOverviewController, self).__init__(parentUI)
        self.setupUi(self)
        self.exportButton.clicked.connect(self.__exportPolicy__)

        self.mainController = mainController

        # Maps each tableGenerator with the index in the tab order: index -> tabGenerator
        self.tablesIndexGeneratorMapping = {}

        # Set all the tabs with the title
        self.tabTitleLabel.setText(
            self.mainController.getModel().getDatabaseModel().dbConfig["database"] + " tables")
        i = 0
        for tableName in self.mainController.getModel().getDatabaseModel().tables:
            tabGenerator = TabGenerator()
            self.tabWidget.addTab(tabGenerator.tab, tableName)
            # signals
            tabGenerator.individualPoliciesTableWidget.cellDoubleClicked.connect(self.__individualPolicieskHandler__)
            self.__connectIndividualRightClickEvent__(tabGenerator.individualPoliciesTableWidget)
            tabGenerator.aggregatePoliciesTableWidget.cellDoubleClicked.connect(self.__aggregatePoliciesHandler__)
            self.__connectAggregateRightClickEvent__(tabGenerator.aggregatePoliciesTableWidget)
            # store the object into the mapping dictionary
            self.tablesIndexGeneratorMapping[i] = tabGenerator
            i = i + 1

    def __importPolicy__(self):
        toImplement = True
        #fname, filters = QFileDialog.getOpenFileName(self, 'Import Policy', '~', 'Policy File (*.xml)')
        # f = open(fname, 'r')
        #
        # with f:
        # data = f.read()
        #     self.textEdit.setText(data)

    def __exportPolicy__(self):
        fname, filters = QFileDialog.getSaveFileName(self, 'Export Policy', '~', 'Policy File (*.xml)')

        xmlWrite = XmlWriter(self.mainController, fname)
        xmlWrite.build()


    def __setStartDateLabel__(self, simplePolicyCreator):
        if simplePolicyCreator.startDateCheckBox.isChecked():
            return QTableWidgetItem(simplePolicyCreator.startDateTimeEdit.date().toString())
        return QTableWidgetItem("Always active")

    def __setEndDateLabel__(self, simplePolicyCreator):
        if simplePolicyCreator.endDateCheckBox.isChecked():
            return QTableWidgetItem(simplePolicyCreator.endDateTimeEdit.date().toString())
        return QTableWidgetItem("Always active")

    def __getCurrentViewObjects__(self):
        selectedTabIndex = self.tabWidget.currentIndex()
        selectedTabGenerator = self.tablesIndexGeneratorMapping[selectedTabIndex]
        selectedTabTitle = self.tabWidget.tabText(selectedTabIndex)
        return selectedTabIndex, selectedTabGenerator, selectedTabTitle

    # ----------------------------------------
    # INDIVIDUAL POLICIES
    # ----------------------------------------

    def __generateSimplePolicy__(self, simplePolicyCreator):
        policyName = simplePolicyCreator.policyName.text()
        startDate = Keywords.MIN_DATE
        endDate = Keywords.MAX_DATE
        userRole = int(simplePolicyCreator.userRoleComboBox.currentText())
        selectedColumns = []
        fromTable = simplePolicyCreator.tableLabel.text()
        joins = {}
        where = simplePolicyCreator.whereTextEdit.toPlainText()

        # set start date
        if simplePolicyCreator.startDateCheckBox.isChecked():
            startDate = simplePolicyCreator.startDateTimeEdit.dateTime().toString(Keywords.DATE_FORMAT)
        # set end date
        if simplePolicyCreator.endDateCheckBox.isChecked():
            endDate = simplePolicyCreator.endDateTimeEdit.dateTime().toString(Keywords.DATE_FORMAT)
        # set selected columns
        for column in simplePolicyCreator.selectCheckBoxes:
            if simplePolicyCreator.selectCheckBoxes[column].isChecked():
                selectedColumns.append(column)
        # set joins
        for joinedTable in simplePolicyCreator.joinFrames:
            if simplePolicyCreator.joinFrames[joinedTable].joinCheckBox.isChecked():
                joins[joinedTable] = simplePolicyCreator.joinFrames[joinedTable].joinLineEdit.text()

        return SimplePolicy(policyName, startDate, endDate, userRole, selectedColumns, fromTable, joins, where)

    # Handle click on individual table
    def __individualPolicieskHandler__(self, row, column):
        # window tab
        selectedTabIndex, selectedTabGenerator, selectedTabTitle = self.__getCurrentViewObjects__()
        selectedTable = selectedTabGenerator.individualPoliciesTableWidget

        # table attributes
        currentTableModel = self.mainController.getModel().getDatabaseModel().tables[selectedTabTitle]

        if selectedTable.item(row, 3) is None:
            # Otherwise Call the simple policy creator
            simplePolicyCreator = SimplePolicyCreatorController(self.mainController, currentTableModel)
            if simplePolicyCreator.exec_():
                # add to the model
                currentTableModel.addSimplePolicy(simplePolicyCreator.policyName.text(),
                    self.__generateSimplePolicy__(simplePolicyCreator))
                # set the view with the input values
                selectedTable.setItem(row, 0, QTableWidgetItem(simplePolicyCreator.userRoleComboBox.currentText()))
                selectedTable.setItem(row, 1, self.__setStartDateLabel__(simplePolicyCreator))
                selectedTable.setItem(row, 2, self.__setEndDateLabel__(simplePolicyCreator))
                selectedTable.setItem(row, 3, QTableWidgetItem(simplePolicyCreator.policyName.text()))
                selectedTable.insertRow(selectedTable.rowCount())
                # print currentTableModel.policies[simplePolicyCreator.policyName.text()].toString()
        else:
            oldPolicyName = selectedTable.item(row, 3).text()
            policyModel = currentTableModel.simplePolicies[oldPolicyName]
            simplePolicyCreator = SimplePolicyCreatorController(self.mainController, currentTableModel, policyModel)
            if simplePolicyCreator.exec_():
                currentTableModel.updateSimplePolicy(oldPolicyName, self.__generateSimplePolicy__(simplePolicyCreator))
                # set the view with the input values
                selectedTable.setItem(row, 0, QTableWidgetItem(simplePolicyCreator.userRoleComboBox.currentText()))
                selectedTable.setItem(row, 1, self.__setStartDateLabel__(simplePolicyCreator))
                selectedTable.setItem(row, 2, self.__setEndDateLabel__(simplePolicyCreator))
                selectedTable.setItem(row, 3, QTableWidgetItem(simplePolicyCreator.policyName.text()))
                # print currentTableModel.policies[simplePolicyCreator.policyName.text()].toString()

    def __connectIndividualRightClickEvent__(self, tableWidget):
        tableWidget.setContextMenuPolicy(Qt.ActionsContextMenu)
        delete = QAction(tableWidget)
        delete.setText("delete")
        delete.triggered.connect(self.__deleteIndividualPolicy__)
        tableWidget.addAction(delete)

    def __deleteIndividualPolicy__(self):
        selectedTabIndex, selectedTabGenerator, selectedTabTitle = self.__getCurrentViewObjects__()
        selectedTable = selectedTabGenerator.individualPoliciesTableWidget
        selectedItems = selectedTable.selectedItems()
        if selectedItems:
            message = "Are you sure to delete this policy? All the related information will be lost"
            cdc = ConfirmationDialogController(message, Keywords.WARNING_IMAGE)
            if cdc.exec_():
                row = selectedTabGenerator.individualPoliciesTableWidget.indexFromItem(selectedItems[0]).row()
                policyName = selectedTable.item(row, 3).text()
                currentTableModel = self.mainController.getModel().getDatabaseModel().tables[selectedTabTitle]
                selectedTable.removeRow(row)
                currentTableModel.deleteSimplePolicy(policyName)

    # ----------------------------------------
    #         AGGREGATE POLICIES
    # ----------------------------------------


    # Handle click on aggregate table
    def __aggregatePoliciesHandler__(self, row, column):
        # window tab
        selectedTabIndex, selectedTabGenerator, selectedTabTitle = self.__getCurrentViewObjects__()
        selectedTable = selectedTabGenerator.aggregatePoliciesTableWidget
        # table attributes
        currentTableModel = self.mainController.getModel().getDatabaseModel().tables[selectedTabTitle]
        if selectedTable.item(row, 3) is None:
            aggregatePolicyCreator = AggregatePolicyCreatorController(self.mainController, currentTableModel)
            if aggregatePolicyCreator.exec_():
                currentTableModel.addAggregatePolicy(aggregatePolicyCreator.policyName.text(),
                    self.__generateAggregatePolicy__(aggregatePolicyCreator))
                selectedTable.setItem(row, 0, QTableWidgetItem(aggregatePolicyCreator.userRoleComboBox.currentText()))
                selectedTable.setItem(row, 1, self.__setStartDateLabel__(aggregatePolicyCreator))
                selectedTable.setItem(row, 2, self.__setEndDateLabel__(aggregatePolicyCreator))
                selectedTable.setItem(row, 3, QTableWidgetItem(aggregatePolicyCreator.policyName.text()))
                selectedTable.insertRow(selectedTable.rowCount())
                # print "User clicked AT Cell (" + str(row) + "," + str(column) + ") on tab " + selectedTabTitle
        else:
            oldPolicyName = selectedTable.item(row, 3).text()
            policyModel = currentTableModel.aggregatePolicies[oldPolicyName]
            aggregatePolicyCreator = AggregatePolicyCreatorController(self.mainController, currentTableModel,
                                                                      policyModel)
            if aggregatePolicyCreator.exec_():
                currentTableModel.updateAggregatePolicy(oldPolicyName,
                                                        self.__generateAggregatePolicy__(aggregatePolicyCreator))
                # set the view with the input values
                selectedTable.setItem(row, 0, QTableWidgetItem(aggregatePolicyCreator.userRoleComboBox.currentText()))
                selectedTable.setItem(row, 1, self.__setStartDateLabel__(aggregatePolicyCreator))
                selectedTable.setItem(row, 2, self.__setEndDateLabel__(aggregatePolicyCreator))
                selectedTable.setItem(row, 3, QTableWidgetItem(aggregatePolicyCreator.policyName.text()))


    def __generateAggregatePolicy__(self, aggregatePolicyCreator):
        policyName = aggregatePolicyCreator.policyName.text()
        startDate = Keywords.MIN_DATE
        endDate = Keywords.MAX_DATE
        userRole = int(aggregatePolicyCreator.userRoleComboBox.currentText())
        selectedColumns = []
        fromTable = aggregatePolicyCreator.tableLabel.text()
        where = aggregatePolicyCreator.whereTextEdit.toPlainText()
        groupBy = aggregatePolicyCreator.groupByLineEdit.text()
        aggregate = aggregatePolicyCreator.aggregatesComboBox.currentText()

        # set start date
        if aggregatePolicyCreator.startDateCheckBox.isChecked():
            startDate = aggregatePolicyCreator.startDateTimeEdit.dateTime().toString(Keywords.DATE_FORMAT)
        # set end date
        if aggregatePolicyCreator.endDateCheckBox.isChecked():
            endDate = aggregatePolicyCreator.endDateTimeEdit.dateTime().toString(Keywords.DATE_FORMAT)
        # set selected columns
        for column in aggregatePolicyCreator.selectCheckBoxes:
            if aggregatePolicyCreator.selectCheckBoxes[column].isChecked():
                selectedColumns.append(column)

        return AggregatePolicy(policyName, startDate, endDate, userRole, selectedColumns, fromTable, where, groupBy, aggregate)

    def __connectAggregateRightClickEvent__(self, tableWidget):
        tableWidget.setContextMenuPolicy(Qt.ActionsContextMenu)
        delete = QAction(tableWidget)
        delete.setText("delete")
        delete.triggered.connect(self.__deleteAggregatePolicy__)
        tableWidget.addAction(delete)

    def __deleteAggregatePolicy__(self):
        selectedTabIndex, selectedTabGenerator, selectedTabTitle = self.__getCurrentViewObjects__()
        selectedTable = selectedTabGenerator.aggregatePoliciesTableWidget
        selectedItems = selectedTable.selectedItems()
        if selectedItems:
            message = "Are you sure to delete this policy? All the related information will be lost"
            cdc = ConfirmationDialogController(message, Keywords.WARNING_IMAGE)
            if cdc.exec_():
                row = selectedTabGenerator.aggregatePoliciesTableWidget.indexFromItem(selectedItems[0]).row()
                policyName = selectedTable.item(row, 3).text()
                currentTableModel = self.mainController.getModel().getDatabaseModel().tables[selectedTabTitle]
                selectedTable.removeRow(row)
                currentTableModel.deleteAggregatePolicy(policyName)
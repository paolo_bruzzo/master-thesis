from PySide.QtGui import QDialog
from PySide.QtGui import QCheckBox
from PySide.QtGui import QApplication
from PySide.QtGui import QMessageBox
from PySide.QtCore import QDateTime

from sources.utils.Keywords import Keywords
from sources.view import SimplePolicyCreator
from sources.controller.subcomponents.JoinFrameController import JoinFrameController
from sources.controller.subcomponents.TestDialogController import TestDialogController
from sources.model.entities.SimplePolicy import SimplePolicy

from dateutil.parser import parse

import mysql.connector


class SimplePolicyCreatorController(QDialog, SimplePolicyCreator.Ui_Dialog):
    def __init__(self, mainController=None, tableModel=None, policyModel=None, parentUI=None):
        super(SimplePolicyCreatorController, self).__init__(parentUI)
        self.setupUi(self)
        self.testButton.clicked.connect(self.__policyIsCorrect__)
        self.userRoleComboBox.currentIndexChanged.connect(self.__initSelectColumns__)

        self.mainController = mainController
        self.tableModel = tableModel
        self.policyModel = policyModel
        self.updatingExistingPolicy = False

        # column -> check box object
        self.selectCheckBoxes = {}
        # tableName -> join frame
        self.joinFrames = {}

        self.__initFields__()

    def __initSelectColumns__(self):
        self.__clearLayout__(self.verticalLayout)
        self.selectCheckBoxes.clear()
        userRole = int(self.userRoleComboBox.currentText())
        for column in self.tableModel.getColumnsNotInSimplePolicies(userRole,self.policyModel):
            self.selectCheckBoxes[column] = self.__addColumnToSelectScrollArea__(column)

    def __clearLayout__(self, layout):
        while layout.count():
            child = layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()
            elif child.layout() is not None:
                self.__clearLayout__(child.layout())

    def __initFields__(self):
        self.tableLabel.setText(self.tableModel.tableName)
        for tableName in self.mainController.getModel().getDatabaseModel().tables:
            if tableName != self.tableModel.tableName:
                self.joinFrames[tableName] = self.__addJoinFrameToJoinScrollArea__(tableName)
        # new policy
        if self.policyModel is None:
            self.__initSelectColumns__()
            self.startDateTimeEdit.setDateTime(QDateTime.currentDateTime())
            self.startDateTimeEdit.setMinimumDateTime(QDateTime.currentDateTime())
            self.endDateTimeEdit.setDateTime(QDateTime.currentDateTime().addYears(3))
            self.endDateTimeEdit.setMinimumDateTime(QDateTime.currentDateTime())
        # updating policy
        else:
            self.updatingExistingPolicy = True
            self.policyName.setText(self.policyModel.policyName)
            if self.userRoleComboBox.currentText() == str(self.policyModel.userRole):
                self.__initSelectColumns__()

            else:
                self.userRoleComboBox.setCurrentIndex(self.userRoleComboBox.findText(str(self.policyModel.userRole)))
            if self.policyModel.startDate == Keywords.MIN_DATE:
                self.startDateCheckBox.setChecked(False)
                self.startDateTimeEdit.hide()
            else:
                sd = parse(self.policyModel.startDate)
                self.startDateTimeEdit.setDateTime(QDateTime(sd.year, sd.month, sd.day, sd.hour, sd.minute, sd.second))
            if self.policyModel.endDate == Keywords.MAX_DATE:
                self.endDateCheckBox.setChecked(False)
                self.endDateTimeEdit.hide()
            else:
                sd = parse(self.policyModel.endDate)
                self.endDateTimeEdit.setDateTime(QDateTime(sd.year, sd.month, sd.day, sd.hour, sd.minute, sd.second))
            for column in self.policyModel.selectColumns:
                 self.selectCheckBoxes[column].setChecked(True)
            for tableName in self.policyModel.joins:
                self.joinFrames[tableName].joinLineEdit.setText(self.policyModel.joins[tableName])
                self.joinFrames[tableName].joinLineEdit.setEnabled(True)
                self.joinFrames[tableName].joinCheckBox.setChecked(True)
            self.whereTextEdit.setText(self.policyModel.where)

    def __addJoinFrameToJoinScrollArea__(self, tableName):
        frame = JoinFrameController()
        frame.tableLabel.setText(tableName)

        self.verticalLayout_2.addWidget(frame)
        return frame

    def __addColumnToSelectScrollArea__(self, columnName):
        columnCheckBox = QCheckBox(self.selectScrollAreaWidgetContents)
        columnCheckBox.setObjectName(columnName + "checkBox")
        self.verticalLayout.addWidget(columnCheckBox)
        columnCheckBox.setText(QApplication.translate("Dialog", columnName, None, QApplication.UnicodeUTF8))
        return columnCheckBox

    def accept(self, *args, **kwargs):
        if self.__fieldsAreAllCorrect__():
            QDialog.accept(self)

    def __policyIsCorrect__(self):
        if self.__fieldsAreAllCorrect__():
            # POLICY
            selectedColumns = []
            fromTable = self.tableLabel.text()
            joins = {}
            where = self.whereTextEdit.toPlainText()
            for col in self.selectCheckBoxes:
                if self.selectCheckBoxes[col].isChecked():
                    selectedColumns.append(col)
            for joinedTable in self.joinFrames:
                if self.joinFrames[joinedTable].joinCheckBox.isChecked():
                    joins[joinedTable] = self.joinFrames[joinedTable].joinLineEdit.text()

            policy = SimplePolicy(None, None, None, None, selectedColumns, fromTable, joins, where)

            # TEST QUERY
            conn = mysql.connector.connect(**self.mainController.getModel().getDatabaseModel().dbConfig)
            cursor = conn.cursor()

            try:
                cursor.execute(policy.toString().replace("current_user_id", "0").replace("current_user_role", "0"))
                tdc = TestDialogController("The policy syntax is correct", policy.previewFormat(),
                                           Keywords.ACCEPTED_IMAGE)
            except mysql.connector.Error, e:
                tdc = TestDialogController(str(e).split(":")[1], policy.previewFormat(), Keywords.DENYED_IMAGE)
            finally:
                # DIALOG
                tdc.exec_()

    def __fieldsAreAllCorrect__(self):
        class EmptyPolicyName(Exception):
            pass

        class ExistingPolicyName(Exception):
            pass

        class NoSelectedItems(Exception):
            pass

        class WrongDates(Exception):
            pass

        class EmptySelectedJoin(Exception):
            pass

        try:
            if self.policyName.text() == "":
                self.policyName.setFocus()
                raise EmptyPolicyName, ("The policy name is mandatory")
            elif not self.updatingExistingPolicy and self.__policyNameAlreadyExists__(self.policyName.text()):
                self.policyName.setFocus()
                raise ExistingPolicyName, ("The policy name already exists")
            elif not self.__atLeastOneChecked__():
                raise NoSelectedItems, ("The query must contain at least one selection")
            elif self.startDateCheckBox.isChecked() and self.endDateCheckBox.isChecked() and self.__datesAreCorrect__(
                    self.startDateTimeEdit, self.endDateTimeEdit):
                raise WrongDates, ("End date must come after start date")
            elif not self.__selectedJoinsAreCorrect__(self.joinFrames):
                raise EmptySelectedJoin, ("If you select a join, you must specify the condition")
            return True

        except EmptyPolicyName, e:
            QMessageBox.warning(self, "Policy error", str(e))
            return False
        except ExistingPolicyName, e:
            QMessageBox.warning(self, "Policy error", str(e))
            return False
        except NoSelectedItems, e:
            QMessageBox.warning(self, "Policy error", str(e))
            return False
        except WrongDates, e:
            QMessageBox.warning(self, "Policy error", str(e))
            return False
        except EmptySelectedJoin, e:
            QMessageBox.warning(self, "Policy error", str(e))
            return False

    def __atLeastOneChecked__(self):
        for checkBox in self.selectCheckBoxes:
            if self.selectCheckBoxes[checkBox].isChecked():
                return True
        return False

    def __policyNameAlreadyExists__(self, policyName):
        for policy in self.tableModel.simplePolicies:
            if policy == policyName:
                return True
        return False

    def __datesAreCorrect__(self, startDateEdit, endDateEdit):
        startDate = startDateEdit.dateTime().toMSecsSinceEpoch()
        endDate = endDateEdit.dateTime().toMSecsSinceEpoch()
        return startDate > endDate

    def __selectedJoinsAreCorrect__(self, joinFrames):
        for joinFrame in joinFrames:
            if joinFrames[joinFrame].joinCheckBox.isChecked() and joinFrames[joinFrame].joinLineEdit.text() == "":
                joinFrames[joinFrame].joinLineEdit.setFocus()
                return False
        return True
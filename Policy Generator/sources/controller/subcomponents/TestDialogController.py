from PySide.QtGui import QDialog
from PySide.QtGui import QPixmap

from sources.view import TestDialog
from sources.utils.Keywords import Keywords


class TestDialogController(QDialog, TestDialog.Ui_Dialog):
    def __init__(self, message="", policy="", imagePath = Keywords.WARNING_IMAGE, parentUI=None):
        super(TestDialogController, self).__init__(parentUI)
        self.setupUi(self)
        self.textBrowser.setText(policy)
        self.messageLabel.setText(message)
        self.iconLabel.setPixmap(QPixmap(imagePath))
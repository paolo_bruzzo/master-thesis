from PySide.QtGui import QDialog
from PySide.QtGui import QPixmap

from sources.view import ConfirmationDialog
from sources.utils.Keywords import Keywords


class ConfirmationDialogController(QDialog, ConfirmationDialog.Ui_Dialog):
    def __init__(self, text="", imagePath = Keywords.WARNING_IMAGE, parentUI=None):
        super(ConfirmationDialogController, self).__init__(parentUI)
        self.setupUi(self)
        self.textLabel.setText(text)
        self.iconLabel.setPixmap(QPixmap(imagePath))
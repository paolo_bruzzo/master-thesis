from PySide.QtGui import QFrame

from sources.view import JoinFrame


class JoinFrameController(QFrame, JoinFrame.Ui_JoinFrame):
    def __init__(self, parentUI=None):
        super(JoinFrameController, self).__init__(parentUI)
        self.setupUi(self)

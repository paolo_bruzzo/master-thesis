from PySide.QtGui import QDialog
from PySide.QtGui import QMessageBox

import mysql.connector
from mysql.connector import errorcode

import re
from sources.view import DatabaseConnector


class DatabaseConnectorController(QDialog, DatabaseConnector.Ui_databaseConnectorDialog):
    def __init__(self, mainController=None, parentUI=None):
        super(DatabaseConnectorController, self).__init__(parentUI)
        self.setupUi(self)
        self.mainController = mainController
        self.connectButton.clicked.connect(self.__connectToDatabase__)

        # TODO remove !!!
        self.__autoFillForm__()

    def __connectToDatabase__(self):
        if not self.__inputIsValid__():
            return
        dbConfig = {
            'user': self.usernameLineEdit.text(),
            'password': self.passwordLineEdit.text(),
            'host': self.hostLineEdit.text(),
            'port': int(self.portLineEdit.text()),
            'database': self.dbNameLineEdit.text()
        }
        try:
            conn = mysql.connector.connect(**dbConfig)
            self.mainController.getModel().getDatabaseModel().setInfo(dbConfig)
            conn.close()
            self.close()
            self.mainController.showPoliciesOverview()

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                QMessageBox.warning(self, "Connection Error",
                                    "Something is wrong with your username or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                QMessageBox.warning(self, "Connection Error", "Database does not exist")
            else:
                QMessageBox.warning(self, "Connection Error", "Hostname or port are incorrect")


    def __inputIsValid__(self):
        if len(self.hostLineEdit.text()) == 0 or len(self.passwordLineEdit.text()) == 0 or len(
                self.hostLineEdit.text()) == 0 or len(self.portLineEdit.text()) == 0 or len(
                self.dbNameLineEdit.text()) == 0:
            QMessageBox.warning(self, "Warning", "Please fill all the input fields")
            return False

        if not re.match('^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])',
                        self.portLineEdit.text()):
            QMessageBox.warning(self, "Warning", "Port number must be an integer number between 0 and 65535")
            return False
        return True

    # TODO: remove, this is just for debugging
    def __autoFillForm__(self):
        self.usernameLineEdit.setText("root")
        self.passwordLineEdit.setText("root")
        self.hostLineEdit.setText("127.0.0.1")
        self.portLineEdit.setText("3306")
        self.dbNameLineEdit.setText("bookstore")
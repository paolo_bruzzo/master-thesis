from PySide.QtGui import QApplication
from sources.controller.DatabaseConnectorController import DatabaseConnectorController
from sources.controller.PoliciesOverviewController import PoliciesOverviewController
from sources.model.Model import Model
import sys

class PolicyGenerator:
    def __init__(self):
        self.app = QApplication(sys.argv)
        self.model = Model()
        self.databaseConnectorForm = DatabaseConnectorController(self)
        self.databaseConnectorForm.show()
        self.app.exec_()

    def getModel(self):
        return self.model

    def showPoliciesOverview(self):
        self.policyOverviewController = PoliciesOverviewController(self)
        self.policyOverviewController.show()

if __name__ == '__main__':
    mainUIController = PolicyGenerator()
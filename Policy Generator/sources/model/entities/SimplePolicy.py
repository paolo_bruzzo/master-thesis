import cgi

class SimplePolicy:
    def __init__(self, policyName, startDate, endDate, userRole, selectColumns, fromTable, joins={}, where=""):
        self.policyName = policyName
        self.startDate = startDate
        self.endDate = endDate
        self.userRole = userRole
        self.selectColumns = selectColumns
        self.fromTable = fromTable
        # table -> condition
        self.joins = joins
        self.where = where

    def toString(self):
        policyToString = ""
        policyToString += "SELECT "
        policyToString += ",".join(column for column in self.selectColumns) + " "
        policyToString += "FROM " + self.fromTable + " "
        for joinTable in self.joins:
            policyToString += "JOIN " + joinTable + " ON " + self.joins[joinTable] + " "
        if self.where != "":
            policyToString += "WHERE " + self.where + " "
        return policyToString

    def previewFormat(self):
        policyToString = ""
        policyToString += "<b>SELECT</b> "
        policyToString += ",".join(column for column in self.selectColumns) + "<br/><br/>"
        policyToString += "<b>FROM</b> " + self.fromTable + "<br/><br/>"
        for joinTable in self.joins:
            policyToString += "<b>JOIN</b> " + joinTable + " <b>ON</b> " + cgi.escape(self.joins[joinTable]) + "<br/><br/>"
        if self.where != "":
            policyToString += "<b>WHERE</b> " + cgi.escape(self.where) + "<br/><br/>"
        return policyToString
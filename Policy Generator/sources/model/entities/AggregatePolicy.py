import cgi

class AggregatePolicy:
    def __init__(self, policyName, startDate, endDate, userRole, selectColumns, fromTable, where="", groupBy="", aggregate=""):
        self.policyName = policyName
        self.startDate = startDate
        self.endDate = endDate
        self.userRole = userRole
        self.selectColumns = selectColumns
        self.fromTable = fromTable
        self.where = where
        self.groupBy = groupBy
        self.aggregate = aggregate

    def toString(self):
        policyToString = ""
        policyToString += "SELECT "
        policyToString += ",".join(self.aggregate+"("+column+")" for column in self.selectColumns) + " "
        policyToString += "FROM " + self.fromTable + " "
        if self.where != "":
            policyToString += "WHERE " + self.where + " "
        if self.groupBy != "":
            policyToString += "GROUP BY " + self.groupBy
        return policyToString

    def previewFormat(self):
        policyToString = ""
        policyToString += "<b>SELECT</b> "
        policyToString += ",".join("<b>"+self.aggregate+"</b>("+column+")" for column in self.selectColumns) + "<br/><br/>"
        policyToString += "<b>FROM</b> " + self.fromTable + "<br/><br/>"
        if self.where != "":
            policyToString += "<b>WHERE</b> " + cgi.escape(self.where) + "<br/><br/>"
        if self.groupBy != "":
            policyToString += "<b>GROUP BY</b> " + self.groupBy
        return policyToString
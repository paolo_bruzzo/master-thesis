class Table:
    def __init__(self, tableName, columns):
        self.tableName = tableName
        self.columns = columns
        self.simplePolicies = {}
        self.aggregatePolicies = {}

    def addSimplePolicy(self, name, policy):
        if name in self.simplePolicies:
            print "Cannot add the policy, it already exists"
        else:
            self.simplePolicies[name] = policy

    def deleteSimplePolicy(self, name):
        del self.simplePolicies[name]

    def updateSimplePolicy(self, name, policy):
        if name in self.simplePolicies:
            del self.simplePolicies[name]
            self.simplePolicies[policy.policyName] = policy
        else:
            print "Cannot update the policy " + name + ", it does not exists"

    def getColumnsNotInSimplePolicies(self, userRole = 0, policyModel=None):
        policyName = None
        if policyModel:
            policyName = policyModel.policyName

        columnsNotInPolicy = []
        for column in self.columns:
            found = False
            for policy in self.simplePolicies:
                if column in self.simplePolicies[policy].selectColumns and self.simplePolicies[policy].userRole == userRole and policy != policyName:
                    found = True
            if not found:
                columnsNotInPolicy.append(column)

        return columnsNotInPolicy

    def addAggregatePolicy(self, name, policy):
        if name in self.aggregatePolicies:
            print "Cannot add the policy, it already exists"
        else:
            self.aggregatePolicies[name] = policy

    def deleteAggregatePolicy(self, name):
        del self.aggregatePolicies[name]

    def updateAggregatePolicy(self, name, policy):
        if name in self.aggregatePolicies:
            del self.aggregatePolicies[name]
            self.aggregatePolicies[policy.policyName] = policy
        else:
            print "Cannot update the policy '" + name + "', it does not exists"

    def getColumnsNotInAggregatePolicies(self, userRole=0, policyModel=None):
        policyName = None
        if policyModel:
            policyName = policyModel.policyName
        columnsNotInPolicy = []

        for column in self.columns:
            found = False
            for policy in self.aggregatePolicies:
                if column in self.aggregatePolicies[policy].selectColumns and self.aggregatePolicies[policy].userRole == userRole and policy != policyName:
                    found = True
            if not found:
                columnsNotInPolicy.append(column)

        return columnsNotInPolicy

    def getBusyRoles(self, policy):
        roles = []
        for policyName in self.aggregatePolicies:
            if not policy or policyName != policy.policyName:
                roles.append(self.aggregatePolicies[policyName].userRole)
        return roles
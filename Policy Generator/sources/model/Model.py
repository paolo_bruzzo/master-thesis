from sources.model.DatabaseModel import DatabaseModel

class Model:
    def __init__(self):
        self.databaseModel = DatabaseModel()

    def getDatabaseModel(self):
        return self.databaseModel
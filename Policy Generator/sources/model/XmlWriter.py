import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

import cgi

class XmlWriter:
    def __init__(self, mainController, fname):
        self.fname = fname
        self.dbModel = mainController.getModel().getDatabaseModel()

    def build(self):
        rootXML = ET.Element("table_list")
        rootXML.set("schema", self.dbModel.dbConfig['database'])

        for tableName in self.dbModel.tables:
            table = self.dbModel.tables[tableName]
            simP = self.dbModel.tables[tableName].simplePolicies
            aggP = self.dbModel.tables[tableName].aggregatePolicies

            if simP or aggP:
                tableXML = ET.SubElement(rootXML, "table")
                tableXML.set("name", tableName)

                columnsXML = ET.SubElement(tableXML, "columns")
                columnsXML.text = ",".join(column for column in table.columns)

                for policyName in simP:
                    policy = simP[policyName]
                    relationViewXML = ET.SubElement(tableXML, "relation_view")
                    relationViewXML.set(cgi.escape("name"), policyName)
                    relationViewXML.set("kind", "simple")

                    userRoleXML = ET.SubElement(relationViewXML, "user_role")
                    userRoleXML.text = str(policy.userRole)

                    beginDateXML = ET.SubElement(relationViewXML, "begin_date")
                    beginDateXML.text = policy.startDate

                    expirationXML = ET.SubElement(relationViewXML, "expiration")
                    expirationXML.text = policy.endDate

                    permissionsXML = ET.SubElement(relationViewXML, "permission")

                    policyXML = ET.SubElement(relationViewXML, "policy")
                    policyXML.text = policy.toString()

                for policyName in aggP:
                    policy = aggP[policyName]
                    relationViewXML = ET.SubElement(tableXML, "relation_view")
                    relationViewXML.set(cgi.escape("name"), policyName)
                    relationViewXML.set("kind", "aggregate")

                    userRoleXML = ET.SubElement(relationViewXML, "user_role")
                    userRoleXML.text = str(policy.userRole)

                    beginDateXML = ET.SubElement(relationViewXML, "begin_date")
                    beginDateXML.text = policy.startDate

                    expirationXML = ET.SubElement(relationViewXML, "expiration")
                    expirationXML.text = policy.endDate

                    permissionsXML = ET.SubElement(relationViewXML, "permission")

                    policyXML = ET.SubElement(relationViewXML, "policy")
                    policyXML.text = policy.toString()

        xml = minidom.parseString(ET.tostring(rootXML, "UTF-8", "xml"))
        indentedString = xml.toprettyxml()
        file = open(self.fname, 'w')
        file.write(indentedString)
        file.close()
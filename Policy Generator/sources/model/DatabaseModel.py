import mysql.connector
from sources.model.entities.Table import Table


class DatabaseModel:
    def __init__(self):
        self.dbConfig = {}
        # table name is the key, list of columns is the object
        self.tables = {}

    def setInfo(self, dbConfig):
        self.dbConfig = dbConfig
        self.__retrieveMetadata__(dbConfig["database"])

    def __retrieveMetadata__(self, dbName):

        # retrieve table names
        conn = mysql.connector.connect(**self.dbConfig)
        cursor = conn.cursor()

        getTableNamesquery = "SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_SCHEMA = '" + dbName + "'"
        cursor.execute(getTableNamesquery)

        tables = []
        for table in cursor:
            tables.append(str(table[0]))

        cursor.close()
        conn.close()

        # for each table, get the column names and append to the dictionary
        for table in tables:
            conn = mysql.connector.connect(**self.dbConfig)
            cursor = conn.cursor()
            #getColumnNamesQuery = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + table + "'"
            getColumnNamesQuery = "SHOW COLUMNS FROM "+table
            cursor.execute(getColumnNamesQuery)
            columns = []
            for column in cursor:
                columns.append(str(column[0]))
            self.tables[table] = Table(table, columns)
            cursor.close()
            conn.close()

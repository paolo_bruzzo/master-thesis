# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ConfirmationDialog.ui'
#
# Created: Wed Apr  1 15:51:53 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(338, 132)
        self.textLabel = QtGui.QLabel(Dialog)
        self.textLabel.setGeometry(QtCore.QRect(90, 20, 231, 61))
        self.textLabel.setText("")
        self.textLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.textLabel.setWordWrap(True)
        self.textLabel.setObjectName("textLabel")
        self.okButton = QtGui.QPushButton(Dialog)
        self.okButton.setGeometry(QtCore.QRect(250, 90, 81, 32))
        self.okButton.setObjectName("okButton")
        self.cancelButton = QtGui.QPushButton(Dialog)
        self.cancelButton.setGeometry(QtCore.QRect(160, 90, 81, 32))
        self.cancelButton.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.cancelButton.setObjectName("cancelButton")
        self.iconLabel = QtGui.QLabel(Dialog)
        self.iconLabel.setGeometry(QtCore.QRect(10, 20, 61, 61))
        self.iconLabel.setObjectName("iconLabel")

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.okButton, QtCore.SIGNAL("clicked()"), Dialog.accept)
        QtCore.QObject.connect(self.cancelButton, QtCore.SIGNAL("clicked()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.okButton.setText(QtGui.QApplication.translate("Dialog", "Delete", None, QtGui.QApplication.UnicodeUTF8))
        self.cancelButton.setText(QtGui.QApplication.translate("Dialog", "Cancel", None, QtGui.QApplication.UnicodeUTF8))

import icons_rc

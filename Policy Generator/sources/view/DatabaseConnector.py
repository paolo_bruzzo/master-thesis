# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DatabaseConnector.ui'
#
# Created: Thu Mar 26 20:54:05 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_databaseConnectorDialog(object):
    def setupUi(self, databaseConnectorDialog):
        databaseConnectorDialog.setObjectName("databaseConnectorDialog")
        databaseConnectorDialog.resize(321, 222)
        self.hostLabel = QtGui.QLabel(databaseConnectorDialog)
        self.hostLabel.setGeometry(QtCore.QRect(20, 30, 42, 16))
        self.hostLabel.setObjectName("hostLabel")
        self.hostLineEdit = QtGui.QLineEdit(databaseConnectorDialog)
        self.hostLineEdit.setGeometry(QtCore.QRect(90, 30, 121, 21))
        self.hostLineEdit.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.hostLineEdit.setAccessibleName("")
        self.hostLineEdit.setText("")
        self.hostLineEdit.setObjectName("hostLineEdit")
        self.portLabel = QtGui.QLabel(databaseConnectorDialog)
        self.portLabel.setGeometry(QtCore.QRect(220, 30, 16, 16))
        self.portLabel.setObjectName("portLabel")
        self.portLineEdit = QtGui.QLineEdit(databaseConnectorDialog)
        self.portLineEdit.setGeometry(QtCore.QRect(240, 30, 61, 21))
        self.portLineEdit.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.portLineEdit.setAccessibleName("")
        self.portLineEdit.setText("")
        self.portLineEdit.setObjectName("portLineEdit")
        self.dbNameLabel = QtGui.QLabel(databaseConnectorDialog)
        self.dbNameLabel.setGeometry(QtCore.QRect(20, 70, 71, 16))
        self.dbNameLabel.setObjectName("dbNameLabel")
        self.dbNameLineEdit = QtGui.QLineEdit(databaseConnectorDialog)
        self.dbNameLineEdit.setGeometry(QtCore.QRect(90, 70, 211, 21))
        self.dbNameLineEdit.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.dbNameLineEdit.setAccessibleName("")
        self.dbNameLineEdit.setText("")
        self.dbNameLineEdit.setPlaceholderText("")
        self.dbNameLineEdit.setObjectName("dbNameLineEdit")
        self.usernameLabel = QtGui.QLabel(databaseConnectorDialog)
        self.usernameLabel.setGeometry(QtCore.QRect(20, 110, 71, 16))
        self.usernameLabel.setObjectName("usernameLabel")
        self.usernameLineEdit = QtGui.QLineEdit(databaseConnectorDialog)
        self.usernameLineEdit.setGeometry(QtCore.QRect(90, 110, 211, 21))
        self.usernameLineEdit.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.usernameLineEdit.setAccessibleName("")
        self.usernameLineEdit.setText("")
        self.usernameLineEdit.setPlaceholderText("")
        self.usernameLineEdit.setObjectName("usernameLineEdit")
        self.passwordLabel = QtGui.QLabel(databaseConnectorDialog)
        self.passwordLabel.setGeometry(QtCore.QRect(20, 150, 71, 16))
        self.passwordLabel.setObjectName("passwordLabel")
        self.passwordLineEdit = QtGui.QLineEdit(databaseConnectorDialog)
        self.passwordLineEdit.setGeometry(QtCore.QRect(90, 150, 211, 21))
        self.passwordLineEdit.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.passwordLineEdit.setAccessibleName("")
        self.passwordLineEdit.setText("")
        self.passwordLineEdit.setEchoMode(QtGui.QLineEdit.Password)
        self.passwordLineEdit.setPlaceholderText("")
        self.passwordLineEdit.setObjectName("passwordLineEdit")
        self.connectButton = QtGui.QPushButton(databaseConnectorDialog)
        self.connectButton.setGeometry(QtCore.QRect(200, 180, 110, 32))
        self.connectButton.setCursor(QtCore.Qt.PointingHandCursor)
        self.connectButton.setObjectName("connectButton")

        self.retranslateUi(databaseConnectorDialog)
        QtCore.QMetaObject.connectSlotsByName(databaseConnectorDialog)
        databaseConnectorDialog.setTabOrder(self.hostLineEdit, self.portLineEdit)
        databaseConnectorDialog.setTabOrder(self.portLineEdit, self.dbNameLineEdit)
        databaseConnectorDialog.setTabOrder(self.dbNameLineEdit, self.usernameLineEdit)
        databaseConnectorDialog.setTabOrder(self.usernameLineEdit, self.passwordLineEdit)

    def retranslateUi(self, databaseConnectorDialog):
        databaseConnectorDialog.setWindowTitle(QtGui.QApplication.translate("databaseConnectorDialog", "Database Connector", None, QtGui.QApplication.UnicodeUTF8))
        self.hostLabel.setText(QtGui.QApplication.translate("databaseConnectorDialog", "Host:", None, QtGui.QApplication.UnicodeUTF8))
        self.hostLineEdit.setPlaceholderText(QtGui.QApplication.translate("databaseConnectorDialog", "127.0.0.1", None, QtGui.QApplication.UnicodeUTF8))
        self.portLabel.setText(QtGui.QApplication.translate("databaseConnectorDialog", ":", None, QtGui.QApplication.UnicodeUTF8))
        self.portLineEdit.setPlaceholderText(QtGui.QApplication.translate("databaseConnectorDialog", "3306", None, QtGui.QApplication.UnicodeUTF8))
        self.dbNameLabel.setText(QtGui.QApplication.translate("databaseConnectorDialog", "DB Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.usernameLabel.setText(QtGui.QApplication.translate("databaseConnectorDialog", "Username:", None, QtGui.QApplication.UnicodeUTF8))
        self.passwordLabel.setText(QtGui.QApplication.translate("databaseConnectorDialog", "Password:", None, QtGui.QApplication.UnicodeUTF8))
        self.connectButton.setText(QtGui.QApplication.translate("databaseConnectorDialog", "Connect", None, QtGui.QApplication.UnicodeUTF8))


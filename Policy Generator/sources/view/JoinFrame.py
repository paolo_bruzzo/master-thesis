# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'JoinFrame.ui'
#
# Created: Sun Mar 29 16:27:54 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_JoinFrame(object):
    def setupUi(self, JoinFrame):
        JoinFrame.setObjectName("JoinFrame")
        JoinFrame.resize(415, 47)
        JoinFrame.setFrameShape(QtGui.QFrame.StyledPanel)
        JoinFrame.setFrameShadow(QtGui.QFrame.Plain)
        JoinFrame.setLineWidth(0)
        self.horizontalLayout = QtGui.QHBoxLayout(JoinFrame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.joinCheckBox = QtGui.QCheckBox(JoinFrame)
        self.joinCheckBox.setObjectName("joinCheckBox")
        self.horizontalLayout.addWidget(self.joinCheckBox)
        self.tableLabel = QtGui.QLabel(JoinFrame)
        self.tableLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.tableLabel.setObjectName("tableLabel")
        self.horizontalLayout.addWidget(self.tableLabel)
        self.onLabel = QtGui.QLabel(JoinFrame)
        self.onLabel.setObjectName("onLabel")
        self.horizontalLayout.addWidget(self.onLabel)
        self.joinLineEdit = QtGui.QLineEdit(JoinFrame)
        self.joinLineEdit.setEnabled(False)
        self.joinLineEdit.setObjectName("joinLineEdit")
        self.horizontalLayout.addWidget(self.joinLineEdit)

        self.retranslateUi(JoinFrame)
        QtCore.QObject.connect(self.joinCheckBox, QtCore.SIGNAL("clicked(bool)"), self.joinLineEdit.setEnabled)
        QtCore.QObject.connect(self.joinCheckBox, QtCore.SIGNAL("clicked()"), self.joinLineEdit.clear)
        QtCore.QMetaObject.connectSlotsByName(JoinFrame)

    def retranslateUi(self, JoinFrame):
        JoinFrame.setWindowTitle(QtGui.QApplication.translate("JoinFrame", "Frame", None, QtGui.QApplication.UnicodeUTF8))
        self.joinCheckBox.setText(QtGui.QApplication.translate("JoinFrame", "JOIN", None, QtGui.QApplication.UnicodeUTF8))
        self.tableLabel.setText(QtGui.QApplication.translate("JoinFrame", "table", None, QtGui.QApplication.UnicodeUTF8))
        self.onLabel.setText(QtGui.QApplication.translate("JoinFrame", "ON", None, QtGui.QApplication.UnicodeUTF8))
        self.joinLineEdit.setPlaceholderText(QtGui.QApplication.translate("JoinFrame", "condition", None, QtGui.QApplication.UnicodeUTF8))


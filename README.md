# README #

This repository contains the tools developed for the Master Thesis: **Inline Views: Protecting against SQL Injection attacks while providing access to Aggregate Values**. The work is the result of a 7 months research carried out at the University of Illinois at Chicago and completed at Politecnico di Milano.

### What is this repository for? ###
The purpose of this work is to introduce an additional protection layer in between the web application and the database level to enhance the protection of web applications against classic SQL injections.

### How does it work ? ###
The current implementation can be used with any JSP web application. The overall system is composed by a front-end **policy generator** and a back-end filter (**custom interceptor**) that is an extension of the MySQL Java connector: the former is used by the web application administrator to generate the policies that will be used to protect the database tables, while the latter is the patch that actually merges the policies and replaces the protected tables.

![alt text](https://bytebucket.org/paolo_bruzzo/master-thesis/raw/0a3753316174119935a7a6d5645f7b4b58f98f92/Custom%20Interceptor/imgages/Interceptor%20Design.png "Custom Interceptor")

The **policy generator** is a user interface that allows to write the policies in a handy way and it’s been implemented to reduce the space of action in which the policy can be written. It generates an XML file that contains the policies and the attributes that will be used by the back-end to modify the queries as discussed. This file is read when the web server is turned on, the policies are taken in input by a module of the back-end that merges them and stores them in memory where they are automatically kept up to date. When the web application sends out a query, the **custom interceptor** catches it, checks if there is any policy in memory that has to be applied to the tables used in the query, and replaces them in case a policy is found. The modified query reaches the database, gets executed, and the result set goes back to the web server through the original MySQL Java connector.

### How to install ? ###
The core part of the entire system is the backend: this is necessary in order to protect the web application:

  * Download the **TestApplications.zip**, unzip it, and upload one of the folders on the web server you are using (tests done on Apache Tomcat 7.0.56)

  * Download the **CustomInterceptor/CustomInterceptor.zip**, unzip it, and place the 3 jar files into the *WebContent/WEB-INF/lib/* path of the web application

  * Import the database schema of the application you have uploaded into your MySQL/MariaDB database

This is enough to have the web application fully patched. In order to patch your own applications, I suggest to read the thesis document to have a deeper understanding of the tiny modifications that your application needs in order to work properly with the interceptor.

In order to write the security policies, we suggest to download the **PolicyGenerator/Runnable.zip**, unzip it, and double click on the application icon. Your system requires Python 2.7 to run it, and a setup file with the source code is also available. For any question about making the backend read your policy XML file, please read Chapter 4 of the thesis.

### Who do I talk to? ###

* Paolo Bruzzo - https://www.linkedin.com/in/paolobruzzo